import React, { Component } from 'react';
import Tabs from './BoardsPageTabs'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Boards from './Boards';
import * as TrelloAPIData from './TrelloAPIData.js'
import './BoardsPage.css'

class BoardsPage extends Component {
    state = { 
        newBoardName: "",
        boardNameText: ""
    }

    handleSubmit = (e) => { e.preventDefault(); this.setState({newBoardName: this.state.boardNameText}, () => {TrelloAPIData.addBoards(this.state.newBoardName)})}
    
    render() { 
        return (
            <div className='bordersPage'>
                <section className='introSection'>
                    <div className='intoSectionLogo'>
                        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6e/D_Magazine_logo.svg" height={'25%'} width={'25%'} alt="" />
                        <h4 className='introSectionHeading'>Deepak Trello Workspace</h4>
                    </div>
                    <Tabs  val={"Home"}/>
                </section>
                <section className='boardsDisplay'>
                <Card style={{ width: '35%', marginLeft: '0.5rem', marginRight: '0.5rem' }} onClick={this.handleShow}>
                <Card.Img variant="top" src="https://picsum.photos/200/100" />
                <Card.Body style={{display:'flex', justifyContent:'center', alignItems:'center', flexDirection:'column'}}>
                    <Card.Title>Create New Board</Card.Title>
                    <Form style={{display:'flex', justifyContent:'center', alignItems:'center', flexDirection:'column'}}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control type="text" placeholder="Enter Board Name" onChange={(e) => {this.setState({boardNameText : e.target.value})}}/>
                        </Form.Group>
                        <Button style={{marginTop: '1rem'}} variant="primary" type="submit" onClick={(e) => {this.handleSubmit(e)} }>
                            Submit
                        </Button>
                    </Form>
                </Card.Body>
                </Card>
                <Boards cards={this.props.cards} />
                </section>    
            </div>
        );
    }
}
 
export default BoardsPage;