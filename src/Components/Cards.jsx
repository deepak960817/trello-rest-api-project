import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Modal from 'react-bootstrap/Modal';
import * as TrelloAPIData from './TrelloAPIData.js'
import CardModal from './CardModal.jsx';

class Cards extends Component {
    
    state = {
        cardList: "",
        show: false,
        setShow: false,
        checklistShow: false,
    }

    handleClose = () => this.setState({show: false, setShow: false});
    handleShow = () => this.setState({show: true, setShow: true});

    componentDidMount(){
        TrelloAPIData.getCards(this.props.id).then(result => this.setState({cardList: result}))
    }

    handleDelete = (id) => {TrelloAPIData.deleteCards(id)};

    handleClick = (e) => {
        e.preventDefault();
        this.setState({checklistShow: true})
    }

    render() { 
        
        return ( this.state.cardList === "" ? <p>Loading</p> :
            this.state.cardList.map((val) => {
            return (
                <div key={val.id}>
                <Card style={{marginTop: '1rem'}}>
                <Card.Body style={{display:'flex', justifyContent:'space-around', flexDirection: 'column', alignItems: 'center'}}>
                    <Card.Title>{val.name}</Card.Title>
                    <div style={{display: 'flex'}}>
                        <Button variant='primary' style={{height:'100%', marginRight: '0.5rem'}} onClick={this.handleShow}>Update</Button>
                        <Button variant='danger' style={{height:'100%', marginLeft: '0.5rem'}} onClick={() => this.handleDelete(val.id)}>Delete</Button>
                    </div> 
                </Card.Body>

                <Modal show={this.state.show} onHide={() => {this.setState({checklistShow: false, show: false})}} size="lg" aria-labelledby="example-modal-sizes-title-lg">
                    <Modal.Header closeButton>
                    <Modal.Title>{val.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{display:'flex'}}>
                        <section style={{width:'70%'}}>
                            {this.state.checklistShow === true ? <CardModal id={val.id}/>:"" }
                        </section>
                        <div style={{display:'flex', width:'30%', justifyContent:'center'}}>
                            <Button onClick={(e) => this.handleClick(e)}>CheckList</Button>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Close
                    </Button>
                    </Modal.Footer>
                </Modal>
                </Card>
                </div>
            );
            }))   
    }
}
 
export default Cards;