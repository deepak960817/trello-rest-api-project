import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import * as TrelloAPIData from './TrelloAPIData.js'
import {Link} from 'react-router-dom';

class Boards extends Component {

    handleDelete = (id) => {TrelloAPIData.deleteBoards(id)}
    handleClick = (val) => {window.location.href=(`${val}`)}

    render() { 
        
        return (
            this.props.cards.map((val) => {
                return(
                    <>
                        <Card key={val.id} style={{ width: '25%', marginLeft: '0.5rem', marginRight: '0.5rem'}} onClick={this.handleShow}>
                        <Card.Img variant="top" src="https://placeimg.com/200/140/tech" />
                        <Card.Body style={{display:'flex', alignItems:'center', flexDirection: 'column'}}>
                            <Card.Title>{val.name}</Card.Title>
                            <div style={{display: 'flex', justifyContent: 'space-around', alignItems:'center', marginTop:'auto', marginBottom:'auto', flexWrap: 'wrap'}}>
                                <Link to={`${val.id}-${val.name}`} onClick={() => this.handleClick(`/${val.id}-${val.name}`)}>
                                <Button variant="primary" type="submit" style={{height:'100%', marginRight: '0.5rem'}}>View Board</Button>
                                </Link>
                                <Button variant="danger" type="submit" style={{height:'100%', marginRight: '0.5rem'}} onClick={() => {this.handleDelete(val.id)}}>Delete Board</Button>
                            </div>
                        </Card.Body>
                        </Card>
                    </>
          )})
        );
    }
}


export default Boards;