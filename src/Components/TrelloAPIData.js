import axios from 'axios';

axios.defaults.params = {
    key : "ce26ffdbf3ab54db085570e359fe543b",
    token: "cc85dd788c5d9ce84a93da1099e4578fbb3333bdaaf3dcaafb57917769251e8c"
}

function refreshPage(val) {window.location.reload(val)};

export function getBoards() {
    return axios.get('https://api.trello.com/1/members/me/boards')
    .then((result) => result.data)
}

export function addBoards(name) {
    return axios.post('https://api.trello.com/1/boards', null, {
        params : {
            name
        }
    }).then(() => {refreshPage(true)}) 
}

export function deleteBoards(id) {
    return axios.delete(`https://api.trello.com/1/boards/${id}`)
    .then(() => {refreshPage(true)}) 
}

export function getLists(id) {
    return axios.get(`https://api.trello.com/1/boards/${id}/lists`)
    .then((result) => result.data) 
}

export function addLists(name, idBoard) {
    return axios.post('https://api.trello.com/1/lists', null, {
        params : {
            name,
            idBoard
        }
    }).then(() => {refreshPage(true)}) 
}

export function deleteLists(id) {
    return axios.put(`https://api.trello.com/1/lists/${id}/closed`, null, {
        params : {
            value : true
        }
    })
    .then(() => {refreshPage(true)}) 
}

export function getCards(id) {
    return axios.get(`https://api.trello.com/1/lists/${id}/cards`)
    .then((result) => (result.data)) 
}

export function addCards(idList, name) {
    return axios.post('https://api.trello.com/1/cards', null, {
        params : {
            idList,
            name
        }
    }).then(() => {refreshPage(true)}) 
}

export function deleteCards(id) {
    return axios.delete(`https://api.trello.com/1/cards/${id}`, null, {
    }).then(() => {refreshPage(true)}) 
}

export function addChecklist(idCard, name) {
    return axios.post('https://api.trello.com/1/checklists', null, {
        params : {
            idCard,
            name
        }
    })
    .then(() => {})
}

export function getChecklists(id) {
    return axios.get(`https://api.trello.com/1/cards/${id}/checklists`)
    .then((result) => (result.data)) 
}

export function deleteChecklist(id) {
    return axios.delete(`https://api.trello.com/1/checklists/${id}`, null, {
    }).then(() => {refreshPage(true)}) 
}
