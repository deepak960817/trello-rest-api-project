import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import ProgressBar from 'react-bootstrap/ProgressBar';
import * as TrelloAPIData from './TrelloAPIData.js'
import ChecklistData from './CheckListData';

class CardModal extends Component {
    state = { 
        itemName: "",
        checklistItemName: "",
        cardModalData: ""
    }

    addChecklistToServer = (id) => {
        this.setState({checklistItemName: this.state.itemName, itemName: ""}, () => {TrelloAPIData.addChecklist(id, this.state.checklistItemName)});
    }


    showChecklist = (id) => {
        return(
            <section>
            <div style={{display:'flex', justifyContent:'space-between', marginBottom:'1rem', alignItems:'center'}}>
                <h5>CheckList</h5>
            </div>
            <ProgressBar style={{marginBottom:'1rem'}} now={0} label={`0%`} />
            {this.state.checklistItemName !== "" ? <ChecklistData id={id} /> : ""}
            {<ChecklistData id={id} />}
            <Form style={{marginBottom:'1rem'}}>
                <Form.Group className="mb-3" controlId="text">
                <Form.Control style={{marginTop:'1rem'}} type="email" value={this.state.itemName} placeholder="Add an Item" onChange={(e) => {this.setState({itemName: e.target.value})}}/>
                </Form.Group>
                <div style={{display:'flex', justifyContent:'flex-start'}}>
                    <Button style={{marginTop:'1rem'}} onClick={() => {this.addChecklistToServer(id)}}>Add</Button>
                </div>
            </Form>
            </section>
        )
    }

    render() { 
        return (
            <div>{this.showChecklist(this.props.id)}</div>
        );
    }
}
 
export default CardModal;