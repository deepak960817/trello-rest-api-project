import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Cards from './Cards'
import * as TrelloAPIData from './TrelloAPIData.js'

class Lists extends Component {
    state = { 
        cardNameText: "",
        newCardName: "",
        cardList : "",
    }
    
    handleDelete = (id) => {TrelloAPIData.deleteLists(id);}
    handleSubmit = (e, id) => { e.preventDefault(); this.setState({newCardName: this.state.cardNameText}, () => {TrelloAPIData.addCards(id, this.state.newCardName)})};
    
    render() { 
        
        return ( 
            this.props.lists.map((val) => {
                return(
                    <React.Fragment key={val.id}>
                    <Card style={{ width: '25%', marginLeft: '0.5rem', marginRight: '0.5rem', backgroundColor: '#FFFACD'}}>
                    <Card.Body style={{display:'flex', justifyContent:'space-around', flexDirection: 'column', alignItems: 'center', flex: '0 0 auto'}}>
                    <Card.Img variant="top" src="https://picsum.photos/200/100" />
                    <Card.Title style={{marginTop: '0.5rem', marginBottom:'0.5rem'}}>{val.name}</Card.Title>
                    <div style={{display: 'flex'}}>
                        <Form style={{display: 'flex', flexDirection: 'column'}}>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Control type="text" placeholder="Enter Card Name" onChange={(e) => {this.setState({cardNameText : e.target.value})}}/>
                            </Form.Group>
                            <Button style={{marginTop: '1rem', height: '50%'}} variant="primary" type="submit" onClick={(e) => {this.handleSubmit(e, val.id)}}>Submit</Button>
                        </Form>
                        <Button variant='danger' style={{height:'100%', marginLeft: '0.5rem'}} onClick={() => this.handleDelete(val.id)}>Delete List</Button>
                    </div> 
                    <Cards id={val.id}/>
                    </Card.Body>
                    </Card>
                    </React.Fragment>
          )})
        );
    }
}


export default Lists;