import React, { Component } from 'react';
import Tabs from './BoardsPageTabs'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Lists from './Lists'
import * as TrelloAPIData from './TrelloAPIData.js'
import './BoardsPage.css'

class ListsPage extends Component {
    state = { 
        newListName: "",
        ListNameText: "",
        Lists: "",
        PageMount: true
    }

    handleSubmit = (e) => { e.preventDefault(); this.setState({newListName: this.state.ListNameText}, () => {TrelloAPIData.addLists(this.state.newListName, this.props.lists[0].idBoard)})}

    
    fetchpath = () => {
        let path = window.location.pathname.split("/")[window.location.pathname.split("/").length-1];
        path = path.split('-')[0];
        return path;
    }

    componentDidMount(){
        let path = this.fetchpath();
        if(path!== "")
        {TrelloAPIData.getLists(path).then(result => this.setState({Lists: result, PageMount: false}))}
    }

    render() { 
        
        return ( this.state.PageMount ? <></> :
            <div className='bordersPage'>
                <section className='introSection'>
                    <div className='intoSectionLogo'>
                        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6e/D_Magazine_logo.svg" height={'25%'} width={'25%'} alt="" />
                        <h4 className='introSectionHeading'>Deepak Trello Workspace</h4>
                    </div>
                    <Tabs val={"Boards"}/>
                </section>
                <section className='boardsDisplay'>
                    <Lists lists={this.state.Lists} />
    
                    <Card style={{ width: '25%', marginLeft: '0.5rem', marginRight: '0.5rem', height:'100%', backgroundColor:'#FFFACD'}}>
                    <Card.Body style={{display:'flex', flexDirection: 'column', alignItems: 'center', flex: '0 0 auto'}}>
                        <Card.Img variant="top" src="https://placeimg.com/200/100/tech" />
                        <Card.Title style={{marginTop: '0.5rem', marginBottom:'0.5rem'}}>Create New List</Card.Title>
                        <Form style={{display: 'flex', flexDirection: 'column'}}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control type="text" placeholder="Enter List Name" onChange={(e) => {this.setState({ListNameText : e.target.value})}}/>
                        </Form.Group>
                        <Button style={{marginTop: '1rem'}} variant="primary" type="submit" onClick={(e) => {this.handleSubmit(e)} }>
                            Submit
                        </Button>
                    </Form>
                    </Card.Body>
                    </Card>
                </section>    
            </div>
        );
    }
}
 
export default ListsPage;