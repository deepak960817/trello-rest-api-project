import React, { Component } from 'react';
import * as TrelloAPIData from './TrelloAPIData.js'
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

class ChecklistData extends Component {
    state = { 
        ChecklistData: ""
     }

    componentDidMount(){
        TrelloAPIData.getChecklists(this.props.id).then((result) => this.setState({ChecklistData:result}));
    }

    handleDelete(id){
        TrelloAPIData.deleteChecklist(id);
    }

    render() { 
        return (this.state.ChecklistData === "" ? <></> :
            this.state.ChecklistData.map((val) => {
            return(
                <Card key={val.id} style={{width:'100%', marginTop:'1rem', backgroundColor:'#E8E8E8'}}>
                <Card.Body>
                    <Card.Title>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label={<b>{val.name}</b>} />
                        </Form.Group>
                    </Card.Title>
                    <Button variant='danger' onClick={() => this.handleDelete(val.id)}>Delete</Button>
                </Card.Body>
                </Card>
                )
            })
        );
    }
}
 
export default ChecklistData;