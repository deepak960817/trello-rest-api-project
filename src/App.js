import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Header from './Components/Header';
import LeftSection from './Components/LeftSection';
import BoardsPage from './Components/BoardsPage';
import ListsPage from './Components/ListsPage';
import * as TrelloAPIData from './Components/TrelloAPIData.js';


class App extends Component {
  state = { 
    PageMount: true,
    Boards : "",
    Lists : "",
   } 

  fetchBoardID = () => { 
        return window.location.pathname.split("/")[window.location.pathname.split("/").length-1];
  }

  componentDidMount(){
    TrelloAPIData.getBoards().then(result => this.setState({Boards : result, PageMount : false}));
  }

  render() { 
    let path = this.fetchBoardID();
    return ( this.state.PageMount ? <div className='loadingImg' style={{display:'flex', justifyContent:'center'}}><img width={'400rem'} src="https://www.kindpng.com/picc/m/19-197708_loading-please-wait-png-transparent-png.png" alt=""/></div> :
      <React.Fragment>
        <section>
          <Header />
          <section style={{display:'flex'}}>
            <Router>
              <Routes>
                <Route path={"/"} element={
                  <>
                  <LeftSection boards={this.state.Boards}/>
                   <BoardsPage cards={this.state.Boards} />
                   </>
                }/>
                <Route path={`/${path}`} element={
                  <>
                  <LeftSection boards={this.state.Boards}/>
                    <ListsPage />
                  </>
                }/>
              </Routes>
            </Router>
            
          </section>
        </section>
        
      </React.Fragment>
    );
  }
}
 
export default App;

